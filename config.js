module.exports = {
    'name'  : 'Blog11',
    'camel' : 'Blog11',
    'slug'  : 'blog-11',
    'dob'   : 'Blog_11_1440',
    'desc'  : 'Grid layout of blog posts with one featured on left and two smaller on right.',
}