class Blog11 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initBlog11()
    }

    initBlog11 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Blog11")
    }

}

window.customElements.define('fir-blog-11', Blog11, { extends: 'div' })
