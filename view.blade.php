<!-- Start Blog11 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Grid layout of blog posts with one featured on left and two smaller on right. -->
@endif
<div class="blog-11"  is="fir-blog-11">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="blog-11__wrap">
    @for($x = 0; $x < 3; $x++)
        @if ($x == 0)
        <a class="blog-11__post-featured" href="#">
          <img class="blog-11__post-img" src="{{ $post['featured_image'] ?: 'https://res.cloudinary.com/response-mktg/image/upload/q_auto,f_auto/v1594221943/fir/demos/img-placeholder.jpg' }}" alt="Placeholder">
          <div class="blog-11__post-content">
            <span>
            <h3 class="blog-11__post-title">{{ $post['title'] ?: $faker->text($maxNbChars = 20) }}</h3>
            <p class="blog-11__post-description">{{ $post['content'] ?: $faker->paragraph($nbSentences = 2, $variableNbSentences = true) }}</p>
            </span>
            <p class="blog-11__post-date">{{ $post['date'] ?: $faker->dayOfMonth($max = 'now') . ' ' . $faker->monthName($max = 'now')  . ' ' . $faker->year($max = 'now') }}</p>
          </div>
        </a>
        @else
          <a class="blog-11__post" href="#">
            <img class="blog-11__post-img" src="{{ $post['featured_image'] ?: 'https://res.cloudinary.com/response-mktg/image/upload/q_auto,f_auto/v1594221943/fir/demos/img-placeholder.jpg' }}" alt="Placeholder">
            <div class="blog-11__post-overlay">
              <h4 class="blog-11__post-title">{{ $post['title'] ?: $faker->text($maxNbChars = 20) }}</h4>
              <p class="blog-11__post-date">{{ $post['date'] ?: $faker->dayOfMonth($max = 'now') . ' ' . $faker->monthName($max = 'now')  . ' ' . $faker->year($max = 'now') }}</p>
            </div>
          </a>
        @endif
    @endfor
  </div>
</div>
<!-- End Blog11 -->
